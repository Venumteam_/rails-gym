class UserAvatarSerializer < ActiveModel::Serializer
  attributes :id, :avatar
end
