source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views


# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

end

group :development do
  gem 'quiet_assets'
  gem 'letter_opener'
end

# For seed data
gem 'database_cleaner'
gem 'faker'

gem 'mini_magick', '4.2.7'
gem 'carrierwave-data-uri'

gem 'kaminari'
gem 'rails-api'
gem 'factory_girl_rails'
gem 'mandrill_mailer'
gem 'launchy'
gem 'devise'
gem 'jwt_authentication', github: 'Rezonans/jwt_authentication', tag: 'v0.0.5'
gem 'cancancan', '~> 1.10'
gem 'active_model_serializers', '0.8.3'
gem 'sprockets', '~> 2.12.3' #need for spa_rails old version
gem 'responders'
gem 'slim'
gem 'angular-rails-templates', github: 'sars/angular-rails-templates'
gem 'spa_rails', github: 'Rezonans/spa_rails'
gem 'bootstrap-sass', '~> 3.3.5'
gem 'ransack'

source 'https://rails-assets.org' do
  gem 'rails-assets-angular-animate'
  gem 'rails-assets-angular'
  gem 'rails-assets-ui-utils'
  gem 'rails-assets-ui-router'
  gem 'rails-assets-restangular', '1.5.1'
  gem 'rails-assets-lodash'
  gem 'rails-assets-angular-bootstrap'
  gem 'rails-assets-bootstrap'
  gem 'rails-assets-angular-ui-select', github: 'sars/angular-ui-select' # https://github.com/angular-ui/ui-select/pull/1068
  gem 'rails-assets-moment'
  gem 'rails-assets-angular-timer'
  gem 'rails-assets-humanize-duration'
  gem 'rails-assets-ngmap'
  gem 'rails-assets-angular-checklist-model'
  gem 'rails-assets-angular-mask'
  gem 'rails-assets-angular-permission'
  gem 'rails-assets-ngInfiniteScroll'
  gem 'rails-assets-ngImgCrop'
  gem 'rails-assets-angular-ui-notification'
  gem 'rails-assets-angular-sanitize'
  gem 'rails-assets-satellizer', '0.11.3'
end
